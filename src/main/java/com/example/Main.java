/*
 * Copyright 2002-2014 the original author or authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.*;
import org.springframework.http.MediaType;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Map;

@Controller
@SpringBootApplication
public class Main {

  @Value("${spring.datasource.url}")
  private String dbUrl;

  @Autowired
  private DataSource dataSource;

  public static void main(String[] args) throws Exception {
    SpringApplication.run(Main.class, args);
  }

  @RequestMapping("/")
  String index(Map<String, Object> model) {
    Rectangle rectangle = new Rectangle();  // creates new rectangle object
    model.put("rectangle", rectangle);
  
    Rectangle deleteRect = new Rectangle();
    model.put("deleteRect", deleteRect);

    // Send database of rectangles' names and colors
    try (Connection connection = dataSource.getConnection()) {
      Statement stmt = connection.createStatement();
      stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Rectangle (id SERIAL PRIMARY KEY NOT NULL, name varchar(30), width real, height real, color varchar(10))");
      ResultSet rs = stmt.executeQuery("SELECT * FROM Rectangle");
      ArrayList<Rectangle> output = new ArrayList<Rectangle>();
      while (rs.next()) {
        Rectangle curRect = new Rectangle();
        curRect.setId(rs.getInt("id"));
        curRect.setName(rs.getString("name"));
        curRect.setWidth(rs.getFloat("width"));
        curRect.setHeight(rs.getFloat("height"));
        curRect.setColor(rs.getString("color"));
        output.add(curRect);
      }
      model.put("rectangleDB", output);
      return "index";
    } catch (Exception e) {
      model.put("message", e.getMessage());
      return "error";
    }
  }

  @PostMapping(
    path = "/rectangle",
    consumes = {MediaType.APPLICATION_FORM_URLENCODED_VALUE}
  )
  public String handleBrowserRectangleSubmit(Map<String, Object> model, Rectangle rectangle) throws Exception {
    // Save the rectangle data into the database
    try (Connection connection = dataSource.getConnection()) {
      Statement stmt = connection.createStatement();
      stmt.executeUpdate("CREATE TABLE IF NOT EXISTS Rectangle (id SERIAL PRIMARY KEY NOT NULL, name varchar(30), width real, height real, color varchar(10))");
      String sql = "INSERT INTO rectangle (name, width, height, color) VALUES ('" + rectangle.getName() + "','" + rectangle.getWidth() + "','" + rectangle.getHeight() + "','" + rectangle.getColor() + "')";
      stmt.executeUpdate(sql);
      System.out.println(rectangle.getName() + " " + rectangle.getWidth() + " " + rectangle.getHeight() + " " + rectangle.getColor());
      return "redirect:/";
    } catch (Exception e) {
      model.put("message", e.getMessage());
      return "error";
    }
  }

  @PostMapping(
    path = "/deleteRectangle"
  )
  public String getdeleteRectangle(Map<String, Object> model, Rectangle deleteRect){
    try (Connection connection = dataSource.getConnection()) {
      Statement stmt = connection.createStatement();
      String sql = "DELETE FROM Rectangle WHERE id = '" + deleteRect.getId() + "'";
      stmt.executeUpdate(sql);
      System.out.println("Deleting rectangle with id " + deleteRect.getId());
      return "redirect:/";
    } catch (Exception e) {
      model.put("message", e.getMessage());
      return "error";
    }
  }

  // Method 1: uses Path variable to specify rectangle
  @GetMapping("/readRectangle/{pid}")
  public String getSpecificRectangle(Map<String, Object> model, @PathVariable String pid){
    try (Connection connection = dataSource.getConnection()) {
      Statement stmt = connection.createStatement();
      String sql = "SELECT * FROM Rectangle WHERE id = '" + pid + "'";
      ResultSet rs = stmt.executeQuery(sql);
      Rectangle rect = new Rectangle();
      if (rs.next()) {
        rect.setId(rs.getInt("id"));
        rect.setName(rs.getString("name"));
        rect.setWidth(rs.getFloat("width"));
        rect.setHeight(rs.getFloat("height"));
        rect.setColor(rs.getString("color"));
      }
      model.put("rect", rect);
      return "readRectangle";
    } catch (Exception e) {
      model.put("message", e.getMessage());
      return "error";
    }
  }

  // // Method 2: uses query string to specify rectangle
  // @GetMapping("/rectangle/read")
  // public String getSpecificRectangle2(Map<String, Object> model, @RequestParam String pid){
  //   System.out.println(pid);
  //   // 
  //   // query DB : SELECT * FROM people WHERE id={pid}
  //   model.put("id", pid);
  //   return "readrectangle";
  // }

  @GetMapping(
    path="/error"
  )
  public String returnError(Map<String, Object> model) {
    return "error";
  }

  @Bean
  public DataSource dataSource() throws SQLException {
    if (dbUrl == null || dbUrl.isEmpty()) {
      return new HikariDataSource();
    } else {
      HikariConfig config = new HikariConfig();
      config.setJdbcUrl(dbUrl);
      return new HikariDataSource(config);
    }
  }

}
