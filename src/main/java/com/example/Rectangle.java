package com.example;

public class Rectangle {
    private int id;
    private String name;
    private float width;
    private float height;
    private String color;

    // Getters
    public int getId() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public float getWidth() {
        return this.width;
    }

    public float getHeight() {
        return this.height;
    }

    public String getColor() {
        return this.color;
    }

    // Setters
    public void setId(int newId) {
        this.id = newId;
    }

    public void setName(String newName) {
        this.name = newName;
    }

    public void setWidth(float newWidth) {
        this.width = newWidth;
    }

    public void setHeight(float newHeight) {
        this.height = newHeight;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }
}